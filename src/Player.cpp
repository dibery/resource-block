#include "Player.h"

int Player::participant = 0, Player::id = 0;

Player::Player()
{
	//ctor
	my_id = id++;
	space[ 0 ] = 10 + 10000 * my_id;
	space[ 1 ] = 0;
	use[ 0 ] = use[ 1 ] = 0;
	sum = 0;
//	cout << "P = " << participant << endl;
//	for ( int i = 0; i < participant; ++i )
//		transfer.push_back( 0 );
	transfer = vector<double>( participant, 0 );
//	cout << "The size of transfer = " << transfer.size() << endl;
	price = vector<double>( participant );
	history = vector<BBU>();
	for ( int i = 0; i < participant; ++i )
		price[ i ] = i == my_id? 0 : 10;
}

Player::~Player()
{
	//dtor
}

Player::Player(const Player& other)
{
	//copy ctor
}

Player& Player::operator=(const Player& rhs)
{
	if (this == &rhs) return *this; // handle self assignment
	//assignment operator
	return *this;
}

double Player::total()
{
	double ret = space[ 0 ] + space[ 1 ];
//	for ( auto i: transfer )
//		if ( i < 0 )
//			ret -= i;
	return ret;
}

double Player::used()
{
	return accumulate( use, use + 2, 0. );
}

double Player::left()
{
	return total() - used();
}

bool Player::ok( double n )
{
	return left() >= n;
}

bool Player::loan( double cpu, Player& other )
{
	if ( !other.ok( cpu ) )
		return false;
//	other.lend( cpu, *this );
	transfer[ other.my_id ] += cpu;
	space[ 1 ] += cpu;
	return true;
}

void Player::lend( double cpu, Player& other )
{
	transfer[ other.my_id ] -= cpu;
	for ( int i = 0; cpu > DBL_EPSILON; ++i )
	{
		int qnt = min( cpu, space[ i ] - use[ i ] );
		cpu -= qnt;
		use[ i ] += qnt;
	}
}

void Player::take( BBU bbu )
{
	bbu.T = high_resolution_clock::now();
	history.push_back( bbu );
	if ( history.size() > 100u )
		cout << "wrong?" << endl;
//	sum += bbu.cpu_req;
	for ( int i = 0; bbu.cpu_req > DBL_EPSILON; ++i )
	{
		if ( i > 1 )
			cout << "";
		int qnt = min( bbu.cpu_req, space[ i ] - use[ i ] );
		bbu.cpu_req -= qnt;
		use[ i ] += qnt;
//		cout << "Iter " << i << "Take out " << qnt << " this time, bbu still has " << bbu.cpu_req << endl;
	}
}

void Player::drop()
{
	if ( history.empty() )
		return;
	for ( auto i = history.begin(); i != history.end(); )
		if ( duration_cast<DUR>( high_resolution_clock::now() - i->T ).count() >= i->timeout )
		{
			for ( int j = 2; i->cpu_req; --j )
			{
				int qnt = min( i->cpu_req, use[ j ] );
				i->cpu_req -= qnt;
				use[ j ] -= qnt;
			}
			auto tt = system_clock::to_time_t( high_resolution_clock::now() );
			cout << "Task " << i->my_id << " is dropped at " << ctime( &tt ) << endl;
			i = history.erase( i );
		}
		else
			++i;
}

double Player::gain()
{
	return sum;
}

void Player::penalty( double n )
{
	sum -= n;
}

double Player::utility()
{
	return used() / total();
}
