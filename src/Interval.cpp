#include "Interval.h"

Interval::Interval( TP b, TP e ) : begin( b ), end( e )
{
	span = duration_cast<DUR>( e - b );
}

Interval::Interval( const Interval& rhs )
{
	begin = rhs.begin;
	end = rhs.end;
	span = rhs.span;
}

Interval::~Interval()
{
	//dtor
}

void Interval::operator= ( const Interval& rhs )
{
	begin = rhs.begin;
	end = rhs.end;
	span = rhs.span;
}
