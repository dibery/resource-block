#include "Server.h"

int Server::id = 0, Server::last_open = -1;
mutex Server::s_mtx;

Server::Server( unsigned cc, double cf, double con, double coff, double v, Type t )
{
    cpu_left = ( cpu_core = cc ) * cf;
    cpu_freq = cf;
    cpu_on = con, cpu_off = coff;
    on = false;
    my_id = ++id;
    fix = cc * cf;
    var = v;
    on_count = 0;
    type = t;
}

Server::Server()
{
	my_id = ++id;
	on = false;
}

Server::~Server()
{
    //dtor
}

void Server::operator= ( const Server& rhs )
{
	cpu_core = rhs.cpu_core,
	cpu_freq = rhs.cpu_freq,
	cpu_on = rhs.cpu_on, cpu_off = rhs.cpu_off,
	cpu_left = rhs.cpu_left,
	on = rhs.on,
//	my_id = rhs.my_id,
	T = rhs.T,
	task = rhs.task,
	history = rhs.history;
	fix = rhs.fix;
	var = rhs.var;
	on_count = rhs.on_count;
	type = rhs.type;
}
bool Server::ok( const BBU& bbu )
{
	return cpu_left >= bbu.cpu_req;
}

void Server::power( bool p )
{
	if ( on == p )
	{
		puts( "Same power status\n" );
		return;
	}
	s_mtx.lock();
//	on = p;
	if( ( on = p ) )
	{
		T = high_resolution_clock::now();
		++on_count;
		auto tt = system_clock::to_time_t( T );
		cout << "Server " << my_id << " turned on at: " << ctime( &tt ) << endl;
		moniter();
	}
	else
	{
		TP now = high_resolution_clock::now();
		auto tt = system_clock::to_time_t( now );
		cout << "Server " << my_id << " turned off at: " << ctime( &tt ) << endl;
		moniter();
	}
	s_mtx.unlock();
}

void Server::take( BBU& bbu )
{
	if ( on == SW_OFF )
		return;
	moniter();
	int qnt = min( bbu.cpu_req, cpu_left );
	cpu_left -= qnt;
//	if ( bbu )
//		bbu.T = high_resolution_clock::now();
	auto tt = system_clock::to_time_t( high_resolution_clock::now() );
	cout << "Server " << my_id << " took BBU " << bbu.my_id << " at: " << ctime( &tt ) << endl;
	task.push_back( bbu );
	moniter();
}

void Server::drop()
{
	if ( on == SW_OFF )
		return;
	s_mtx.lock();
	moniter();
	for ( auto i = task.begin(); i != task.end(); )
		if ( duration_cast<DUR>( high_resolution_clock::now() - i->T ).count() >= i->timeout )
		{
			cpu_left += i->cpu_req;
			auto tt = system_clock::to_time_t( high_resolution_clock::now() );
			cout << "Task " << i->my_id << " is dropped at " << ctime( &tt ) << endl;
			i = task.erase( i );
		}
		else
			++i;
	moniter();
	s_mtx.unlock();
}

bool Server::move( Task_it t, Server& s )
{
	s_mtx.lock();
	moniter();
	cout << "Move BBU " << t->my_id << " from server " << my_id << " to server " << s.my_id << endl << endl;
	int qnt = min( t->cpu_req, s.cpu_left );
	s.take( *t );
	cpu_left += t->cpu_req;
	task.erase( t );
	s_mtx.unlock();
}

double Server::cpu_utility()
{
	return ( cpu_core * cpu_freq - cpu_left ) / ( cpu_core * cpu_freq );
}


double Server::cost()
{
	long long ret = 0;

	for ( auto& i: history ) // i is pair<interval,double>
		ret += var * i.first.length() * i.second;

	return ret + fix * on_count;
}

void Server::moniter()
{
	if ( history.empty() )
		history.push_back( pair<Interval,double>( Interval( T, high_resolution_clock::now() ), cpu_utility() ) );
	else
		history.push_back( pair<Interval,double>( Interval( history.back().first.end, high_resolution_clock::now() ), cpu_utility() ) );
}
