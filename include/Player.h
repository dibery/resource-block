#ifndef PLAYER_H
#define PLAYER_H
#include "common.h"

class Player
{
	public:
		Player();
		~Player();
		Player(const Player& other);
		Player& operator=(const Player& other);
		void take( BBU ), drop(), penalty( double );
		bool loan( double, Player& );
		double left(), total(), used(), gain(), utility();
		bool ok( double );
		bool empty() { return used() == 0; }
		static int participant;

	protected:

	private:
		void lend( double, Player& );
		double space[ 2 ], use[ 2 ], sum;
		vector<BBU> history;
		vector<double> transfer, price;	// transfer[ i ] > 0 means borrow from i
		static int id;
		int my_id;
};

#endif // PLAYER_H
