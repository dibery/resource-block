#ifndef SERVER_H
#define SERVER_H

#include"common.h"

class Server
{
private:
	typedef list<BBU> Task;
	typedef Task::iterator Task_it;
    bool on;

public:
    enum Type{ LIC, RLIC };
    Server();
    Server( unsigned, double, double, double, double, Type );
    virtual ~Server();

//member functions
    bool ok( const BBU& );
    bool empty() { return task.empty(); }
    bool power() const { return on; }
    bool low() { return cpu_utility() < cpu_off; }
    bool high() { return cpu_utility() > cpu_off; }
    void power( bool p );
    void take( BBU& );
    void drop();
    void erase( Task_it i ) { task.erase( i ); }
    bool move( Task_it, Server& );
    void moniter();
	double cpu_utility();
    double cost(); // Find fix & variable costs here
    time_t time_on();
    time_t time_off();
    Task_it begin() { return task.begin(); }
    Task_it end() { return task.end(); }
    void operator= ( const Server& );

//data members
    unsigned cpu_core; // GB
    double cpu_freq; // GHz, % for os
    double cpu_on, cpu_off;
    double cpu_left;
    double fix, var; // fixed / variable cost
    int my_id, on_count;
    Type type;
    TP T;
    Task task;
    vector<pair<Interval,double>> history;

    static int id, last_open;
    static const bool SW_ON = true, SW_OFF = false;
    static mutex s_mtx;
};

#endif // SERVER_H
