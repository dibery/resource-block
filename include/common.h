#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include<bits/stdc++.h>
using namespace std;
using namespace std::chrono;

typedef chrono::high_resolution_clock::time_point TP;
typedef chrono::duration<long long,std::milli> DUR;

class BBU;
class Interval;
class Server;

#include "BBU.h"
#include "Interval.h"
#include "Queue.h"
#include "Server.h"
#include "Player.h"

#endif // COMMON_H_INCLUDED
