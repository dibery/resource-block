#include "common.h"

const int PLAYER_NUM = 2, TASK_NUM = 20, NPOS = -1, RUNTIME = 2, PENALTY = 10;
const double CAP_RATE = 0;
// System initialization
int level[ PLAYER_NUM ];
bool finish = false;
Player* p = nullptr;
Queue<BBU>* waiting = nullptr;
mutex mtx;
FILE *f, *f2;

void exec( const int sid )
{
	mtx.lock();
	mtx.unlock();

	while( !finish || !p[ sid ].empty() )
	{
		mtx.lock();
		if ( p[ sid ].empty() )
			mtx.unlock();
		// Remove my completed tasks
		else
			p[ sid ].drop(), printf( "Utility of server %d is %.0f%%\n", sid, p[ sid ].utility() * 100 ), mtx.unlock();
		this_thread::sleep_for( milliseconds( 25 ) );
	}

	// All tasks are done now
	mtx.lock();
	cout << "Server " << sid + 1 << " joined" << endl;
	mtx.unlock();
}

void wait( int sid )
{
	//task initialization
	srand( sid );
	mtx.lock();
	mtx.unlock();
	time_t t = time( nullptr );

	while( !waiting[ sid ].empty() || time( nullptr ) < t + RUNTIME )
	{
		this_thread::sleep_for( milliseconds( rand() % 50 ) );
		mtx.lock();
		if ( time( nullptr ) < t + RUNTIME && rand() % 4 == 0 )
			for ( int qnt = rand() % 3, i = 0; i < qnt; ++i )
				waiting[ sid ].push( BBU( rand() % 8 + 1, rand() % 2000 + 1000, rand() % 2000 + 1000 ) ); // CPU, Timeout (ms)
		mtx.unlock();

		if ( !waiting[ sid ].empty() )
		{
			BBU& f = waiting[ sid ].front();

			// Expired before it can enter a server
			if ( Interval( f.TQ, high_resolution_clock::now() ).length() > f.q_timeout )
			{
				waiting[ sid ].pop();
				++waiting[ sid ].blocked_bbu;
//				cout << Interval( f.TQ, high_resolution_clock::now() ).length() << endl;
				printf( "BBU %d expired in queue...\n", f.my_id );
				p[ sid ].penalty( PENALTY );
				continue;
			}

			// Take BBU front queue assigned into server i, pop queue
			mtx.lock();

			if ( p[ sid ].ok( f.cpu_req ) )
			{
				p[ sid ].take( f );
				printf( "insert %.0f to server %d\n", f.cpu_req, sid );
				printf( "Utility of server %d is %d%%\n", sid, int( p[ sid ].utility() * 100 ) );
				waiting[ sid ].pop();
			}
			else if ( p[ sid ].loan( f.cpu_req, p[ !sid ] ) )
			{
				p[ sid ].take( f );
				printf( "insert %.0f to server %d by borrowing some\n", f.cpu_req, sid );
				printf( "Utility of server %d is %d%%\n", sid, int( p[ sid ].utility() * 100 ) );
				waiting[ sid ].pop();
			}
			else
				cout << "@@" << endl;
			mtx.unlock();
		}
	}
	mtx.lock();
	cout << "Queue stopped" << endl;
	mtx.unlock();
	finish = true;
}

void simulation()
{
	srand( 0 );
	//TP start_time = high_resolution_clock::now();
	finish = false;
	Player::participant = PLAYER_NUM;
	p = new Player[ PLAYER_NUM ];
	thread world[ PLAYER_NUM ], customer[ PLAYER_NUM ];
	waiting = new Queue<BBU>[ PLAYER_NUM ];

	//server initialization
	mtx.lock();
	for ( int i = 0; i < PLAYER_NUM; ++i )
		world[ i ] = thread( exec, i ), customer[ i ] = thread( wait, i );
	mtx.unlock();

	for ( int i = 0; i < PLAYER_NUM; ++i )
		world[ i ].join(), customer[ i ].join();
	puts( "All tasks are done now. Calculate gain..." );
	int total = 0;
	for ( int i = 0; i < PLAYER_NUM; ++i )
		total += p[ i ].gain();
	for ( int i = 0; i < PLAYER_NUM; ++i )
		cout << "gain of server " << i + 1 << " = " << p[ i ].gain() << endl;
//	fprintf( stdout, "%d\n", total ),
//	fprintf( f, "%d\n", total );	// total gain
	for ( int i = 0; i < PLAYER_NUM; ++i )
	printf( "%d BBU(s) expired in queue\n", waiting[ i ].blocked_bbu );
//	fprintf( f2, "%d\n", waiting.blocked_bbu );
	cout << endl;

	delete[] p;
	delete[] world;
	delete[] waiting;
}

void type( int types, int depth = 0, int filled = 0 )
{
	if ( depth < types )
		for ( int i = 0; i + filled <= PLAYER_NUM; ++i )
		{
			for ( int j = filled; j < filled + i; ++j )
			{
				level[ j ] = depth;
			}

			type( types, depth + 1, filled + i );
		}
	else if ( filled == PLAYER_NUM )
		simulation();
//		for ( int i = 0; i < PLAYER_NUM; ++i )
//			printf( "%d%c", level[ i ], i == PLAYER_NUM - 1? '\n' : '\t' );
}

int main( int argc, char* argv[] )
{
	f = stdout;
	f2 = stdout;
//	if ( argc == 2 )
//		f = fopen( argv[ 1 ], "w" ), f2 = fopen( "drop.txt", "w" );
//	else if ( argc == 1 )
//		f = fopen( "cost.txt", "w" ), f2 = fopen( "drop.txt", "w" );
//	else if ( argc == 3 )
//		f2 = fopen( argv[ 2 ], "w" ), f = fopen( argv[ 1 ], "w" );
	type( 1 );
}
