all:
	g++ -std=c++11 -pthread -g -Wall main.cpp src/*.cpp -I./include -o model3
other:
	g++ -std=c++11 -g -Wall include/BBU.h src/BBU.cpp -o obj/Debug/BBU.o -c
	g++ -std=c++11 -g -Wall include/Server.h src/Server.cpp -o obj/Debug/Server.o -c
	g++ -std=c++11 -g -Wall include/Interval.h src/Interval.cpp -o obj/Debug/Interval.o -c
	g++ -std=c++11 -g -Wall include/Queue.h src/Queue.cpp -o obj/Debug/Queue.o -c
	g++ -std=c++11 -g -Wall main.cpp obj/Debug/*.o -o bin/Debug/model3.exe
